/***********************************************************************
**
** Copyright (C) 2020 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
***********************************************************************/

#include "lpms_me1.h"

#include <SPI.h>
#include <Wire.h>


LPMS_ME1::LPMS_ME1()
{
#if defined ME1_USE_SPI

//      pinMode(ME1_SPI_SCK, OUTPUT);
//      pinMode(ME1_SPI_MISO, INPUT);
//      pinMode(ME1_SPI_MOSI, OUTPUT);
//      pinMode(ME1_SPI_CS, OUTPUT);
//      digitalWrite(ME1_SPI_CS, HIGH);
//      SPI.beginTransaction(SPISettings(ME1_SPI_FREQUENCY, MSBFIRST, SPI_MODE0));

    SPI.begin();

#elif defined ME1_USE_I2C

    Wire.begin();

#endif
}

void LPMS_ME1::setup(void)
{
    set_config_enable();
    wait_update();

    set_data_output(LPMS_ALL_DATA_EN);
    wait_update();

    set_data_freq(LPMS_DATA_FREQ_100HZ);
    wait_update();

    set_acc_range(LPMS_ACC_FS_4G);
    wait_update();

    set_gyr_range(LPMS_GYR_FS_2000DPS);
    wait_update();

    set_mag_rang(LPMS_MAG_FS_8GAUSS);
    wait_update();
     
    // Some other configurations here

    set_config_disable();
    wait_update();
}

void LPMS_ME1::set_config_enable(void)
{
    uint8_t data = 0x80;
    write_register(FUN_CONFIG, &data, 1);
}

void LPMS_ME1::set_config_disable(void)
{
    uint8_t data = 0x00;
    write_register(FUN_CONFIG, &data, 1);
}

void LPMS_ME1::set_data_output(uint8_t data_en)
{
    uint8_t data = data_en & LPMS_DATA_EN_MASK;
    write_register(DATA_ENABLE, &data, 1);
}


me1_status_t LPMS_ME1::set_data_freq(uint8_t freq)
{
    if (freq != LPMS_DATA_FREQ_5HZ &&
        freq != LPMS_DATA_FREQ_10HZ &&
        freq != LPMS_DATA_FREQ_50HZ &&
        freq != LPMS_DATA_FREQ_100HZ &&
        freq != LPMS_DATA_FREQ_200HZ &&
        freq != LPMS_DATA_FREQ_400HZ &&
        freq != LPMS_DATA_FREQ_500HZ &&
        freq != LPMS_DATA_FREQ_800HZ &&
        freq != LPMS_DATA_FREQ_1000HZ)
        return ME1_ERROR;

    uint8_t data = 0;
    read_register(DATA_CTRL, &data, 1);
    data &= ~LPMS_DATA_FREQ_MASK;
    data |= freq;
    write_register(DATA_CTRL, &data, 1);
    return ME1_OK;
}

me1_status_t LPMS_ME1::set_acc_range(uint8_t range)
{
    if (range != LPMS_ACC_FS_2G &&
        range != LPMS_ACC_FS_4G &&
        range != LPMS_ACC_FS_8G &&
        range != LPMS_ACC_FS_16G)
        return ME1_ERROR;

    uint8_t data = 0;
    read_register(CTRL_0_A, &data, 1);
    data &= ~LPMS_ACC_FS_MASK;
    data |= range;
    write_register(CTRL_0_A, &data, 1);
    return ME1_OK;
}


me1_status_t LPMS_ME1::set_gyr_range(uint8_t range)
{
    if (range != LPMS_GYR_FS_125DPS &&
        range != LPMS_GYR_FS_245DPS &&
        range != LPMS_GYR_FS_500DPS &&
        range != LPMS_GYR_FS_1000DPS &&
        range != LPMS_GYR_FS_2000DPS)
        return ME1_ERROR;

    uint8_t data = 0;
    read_register(CTRL_1_G, &data, 1);
    data &= ~LPMS_GYR_FS_MASK;
    data |= range;
    write_register(CTRL_1_G, &data, 1);
    return ME1_OK;
}

me1_status_t LPMS_ME1::set_mag_rang(uint8_t range)
{
    if (range != LPMS_MAG_FS_4GAUSS &&
        range != LPMS_MAG_FS_8GAUSS &&
        range != LPMS_MAG_FS_12GAUSS &&
        range != LPMS_MAG_FS_16GAUSS )
        return ME1_ERROR;
        
    uint8_t data = 0;
    read_register(CTRL_2_M, &data, 1);
    data &= ~LPMS_GYR_FS_MASK;
    data |= range;
    write_register(CTRL_2_M, &data, 1);
    return ME1_OK;   
}


void LPMS_ME1::get_time(float *ts)
{
    uint8_t buf[4] = { 0 };
    read_register(TIMESTAMP_0, buf, 4);
    *ts = uint8_to_float(buf);
}


void LPMS_ME1::get_acc(float *acc)
{
    uint8_t buf[12] = { 0 };
    read_register(ACC_X_0, buf, 12);
    for (int i = 0; i < 3; i++) {
        acc[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_ME1::get_gyr(float *mag)
{
    uint8_t buf[12] = { 0 };
    read_register(GYR_X_0, buf, 12);
    for (int i = 0; i < 3; i++) {
        mag[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_ME1::get_mag(float *mag)
{
   uint8_t buf[12] = { 0 };
   read_register(MAG_X_0, buf, 12);
   for (int i = 0; i < 3; i++) {
        mag[i] = uint8_to_float(&buf[i*4]);
    } 
}

void LPMS_ME1::get_quat(float *quat)
{
    uint8_t buf[16] = { 0 };
    read_register(QUAT_W_0, buf, 16);
    for (int i = 0; i < 4; i++) {
        quat[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_ME1::get_euler(float *euler)
{
    uint8_t buf[12] = { 0 };
    read_register(EULER_X_0, buf, 12);
    for (int i = 0; i < 3; i++) {
        euler[i] = uint8_to_float(&buf[i*4]);
    }
}

void LPMS_ME1::get_firmware_version(uint8_t *firmware_version)
{
    uint8_t buf[2];
    read_register(FIRMWARE_VER_0, buf, 2);
    for (int i = 0; i < 2; i++) {
        firmware_version[i] = buf[i];
    }
}

void LPMS_ME1::get_reg_data(uint8_t addr, uint8_t *buf, uint8_t len)
{
    read_register(addr, buf, len);
}



float LPMS_ME1::uint8_to_float(uint8_t *pu8vals)
{
    data_decoder decoder;
    for(int i = 0; i < 4; i++) {
        decoder.u8vals[i] = *(pu8vals +i);
    }
    return decoder.fval;
}

uint32_t LPMS_ME1::uint8_to_uint32(uint8_t *pu8vals)
{
    data_decoder decoder;
    for(int i = 0; i < 4; i++) {
        decoder.u8vals[i] = *(pu8vals +i);
    }
    return decoder.u32val;
}



void LPMS_ME1::read_register(uint8_t addr, uint8_t *buf, uint8_t len)
{
#if defined ME1_USE_SPI

    addr |= 0x80;
    digitalWrite(ME1_SPI_CS, LOW);
    delayMicroseconds(10);

    SPI.transfer(addr);
    delayMicroseconds(10);
    for (int i = 0; i < len; i++) {
        buf[i] = SPI.transfer(0x00);
    }

    digitalWrite(ME1_SPI_CS, HIGH);
    delayMicroseconds(10);

#elif defined ME1_USE_I2C

    Wire.beginTransmission(ME1_I2C_ADRRESS);
    Wire.write(addr);
    Wire.endTransmission();
    Wire.requestFrom(ME1_I2C_ADRRESS, len);

    int i = 0;
    while(Wire.available()) {
        buf[i++] = Wire.read();
    }

#endif
}

void LPMS_ME1::write_register(uint8_t addr, uint8_t *buf, uint8_t len)
{
#if defined ME1_USE_SPI

    digitalWrite(ME1_SPI_CS, LOW);
    delayMicroseconds(10);

    SPI.transfer(addr);
    delayMicroseconds(10);
    for (int i = 0; i < len; i++) {
        SPI.transfer(buf[i]);
    }
   
    digitalWrite(ME1_SPI_CS, HIGH);
    delayMicroseconds(10);

#elif defined ME1_USE_I2C

    Wire.beginTransmission(ME1_I2C_ADRRESS);
    Wire.write(addr);
    Wire.write(buf, len);
    Wire.endTransmission();

#endif
}

void LPMS_ME1::wait_update(void)
{
    delay(2);  // wait at least 2ms for updating registers
}
void LPMS_ME1::wait_reset(void)
{
    delay(10); // wait at least 10ms for sensor reset
}
void LPMS_ME1::wait_reboot(void)
{
    delay(80); // wait at least 80ms for sensor reboot
}
