#include "Arduino.h"
#include "lpms_me1.h"

LPMS_ME1 lpms_me1;
sensor_data_t sensor_data;

void me1_config_test(void);
void me1_read_data(void);
void me1_print_data(void);

void setup() {
  
    Serial.begin(115200);
    lpms_me1.setup();   
    
}

void loop() {
    me1_read_data();
    me1_print_data();
    delay(1000); 
     
}


void me1_config_test(void)
{
    uint8_t buf[10] = { 0 };
    lpms_me1.get_reg_data(0x00, buf, 10);
    for (int i = 0; i < 10; i++) {
        if (buf[i] < 0x10) {
            Serial.print("0");
            Serial.print(buf[i], HEX);
        } else {
            Serial.print(buf[i], HEX);
        }
        Serial.print(" ");
        
    }
    Serial.println();

    lpms_me1.setup();

    lpms_me1.get_reg_data(0x00, buf, 10);
    for (int i = 0; i < 10; i++) {
        if (buf[i] < 0x10) {
            Serial.print("0");
            Serial.print(buf[i], HEX);
        } else {
            Serial.print(buf[i], HEX);
        }
        Serial.print(" ");
    }
    Serial.println();
    Serial.println();
}

void me1_read_data(void)
{
    lpms_me1.get_time(&sensor_data.time);
    lpms_me1.get_acc(sensor_data.acc);
    lpms_me1.get_gyr(sensor_data.gyr);
    lpms_me1.get_mag(sensor_data.mag);
    lpms_me1.get_quat(sensor_data.quat);
    lpms_me1.get_euler(sensor_data.euler);
    lpms_me1.get_firmware_version(sensor_data.firmware_version);
}
void me1_print_data(void)
{
    uint8_t firmware_ver_2 = sensor_data.firmware_version[0];
    uint8_t firmware_ver_1 = sensor_data.firmware_version[1] &0x0F;
    uint8_t firmware_ver_0 = (sensor_data.firmware_version[1]>>4) &0x0F;
    
    Serial.print("time: ");
    Serial.println(sensor_data.time, 3);

    Serial.print("acc: ");
    Serial.print(sensor_data.acc[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.acc[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.acc[2], 3);
    Serial.println();

    Serial.print("gyr: ");
    Serial.print(sensor_data.gyr[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.gyr[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.gyr[2], 3);
    Serial.println();
    
    Serial.print("mag: ");
    Serial.print(sensor_data.mag[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.mag[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.mag[2], 3);
    Serial.println();


    Serial.print("quat: ");
    Serial.print(sensor_data.quat[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.quat[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.quat[2], 3);
    Serial.print(", ");
    Serial.print(sensor_data.quat[3], 3);
    Serial.println();

    Serial.print("euler: ");
    Serial.print(sensor_data.euler[0], 3);
    Serial.print(", ");
    Serial.print(sensor_data.euler[1], 3);
    Serial.print(", ");
    Serial.print(sensor_data.euler[2], 3);
    Serial.println();
    
    Serial.print("firmware version: ");
    Serial.print(firmware_ver_1,HEX);
    Serial.print(".");
    Serial.print(firmware_ver_0,HEX);
    Serial.print(".");
    Serial.print(firmware_ver_2,HEX);
    Serial.println();

    Serial.println();
}
