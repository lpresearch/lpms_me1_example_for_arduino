/***********************************************************************
**
** Copyright (C) 2020 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
**
***********************************************************************/

#ifndef LPMS_ME1_H
#define LPMS_ME1_H

#include "Arduino.h"

/* Interface selection */
#define ME1_USE_SPI
//#define ME1_USE_I2C


/* SPI interface configuration */
#define ME1_SPI_SCK                 13
#define ME1_SPI_MISO                12
#define ME1_SPI_MOSI                11
#define ME1_SPI_CS                  10

//This pin is used Mega2560
//#define ME1_SPI_SCK                 52
//#define ME1_SPI_MISO                50
//#define ME1_SPI_MOSI                51
//#define ME1_SPI_CS                  53

#define ME1_SPI_FREQUENCY           40000000

/* I2C interface configuration */ 
#define ME1_I2C_SCL                 19
#define ME1_I2C_SDA                 18
#define ME1_I2C_FREQUENCY           400000
#define ME1_I2C_ADRRESS             0x32



/*Memory map*/
#define LPMS_SYS_REG_BASE           0
#define LPMS_SYS_REG_SIZE           128
#define LPMS_SYS_BASE               LPMS_SYS_REG_BASE
#define LPMS_SYS_SIZE               32
#define LPMS_DATA_BASE              (LPMS_SYS_REG_BASE+LPMS_SYS_SIZE)
#define LPMS_DATA_SIZE              84
#define LPMS_INFO_BASE              (LPMS_SYS_REG_BASE+LPMS_SYS_SIZE+LPMS_DATA_SIZE)
#define LPMS_INFO_SIZE              (LPMS_SYS_REG_SIZE-LPMS_SYS_SIZE-LPMS_DATA_SIZE)


/*
  *System Registers{
  */

//Config registers
#define FUN_CONFIG                  (LPMS_SYS_BASE + 0)
#define SYS_CONFIG                  (LPMS_SYS_BASE + 1)
#define DATA_CTRL                   (LPMS_SYS_BASE + 2)
#define DATA_ENABLE                 (LPMS_SYS_BASE + 3)
#define CTRL_0_A                    (LPMS_SYS_BASE + 4)
#define CTRL_1_G                    (LPMS_SYS_BASE + 5)
#define CTRL_2_M                    (LPMS_SYS_BASE + 6)
#define STATUS                      (LPMS_SYS_BASE + 7)
#define FILTER_CONFIG               (LPMS_SYS_BASE + 8)
#define OFFSET_SETTING              (LPMS_SYS_BASE + 9)


//Data registers
#define TIMESTAMP_0                 (LPMS_DATA_BASE + 0)
#define TIMESTAMP_1                 (LPMS_DATA_BASE + 1)
#define TIMESTAMP_2                 (LPMS_DATA_BASE + 2)
#define TIMESTAMP_3                 (LPMS_DATA_BASE + 3)
#define ACC_X_0                     (LPMS_DATA_BASE + 4)
#define ACC_X_1                     (LPMS_DATA_BASE + 5)
#define ACC_X_2                     (LPMS_DATA_BASE + 6)

#define ACC_X_3                     (LPMS_DATA_BASE + 7)
#define ACC_Y_0                     (LPMS_DATA_BASE + 8)
#define ACC_Y_1                     (LPMS_DATA_BASE + 9)
#define ACC_Y_2                     (LPMS_DATA_BASE + 10)
#define ACC_Y_3                     (LPMS_DATA_BASE + 11)
#define ACC_Z_0                     (LPMS_DATA_BASE + 12)
#define ACC_Z_1                     (LPMS_DATA_BASE + 13)
#define ACC_Z_2                     (LPMS_DATA_BASE + 14)
#define ACC_Z_3                     (LPMS_DATA_BASE + 15)
#define GYR_X_0                     (LPMS_DATA_BASE + 16)
#define GYR_X_1                     (LPMS_DATA_BASE + 17)
#define GYR_X_2                     (LPMS_DATA_BASE + 18)
#define GYR_X_3                     (LPMS_DATA_BASE + 19)
#define GYR_Y_0                     (LPMS_DATA_BASE + 20)
#define GYR_Y_1                     (LPMS_DATA_BASE + 21)
#define GYR_Y_2                     (LPMS_DATA_BASE + 22)
#define GYR_Y_3                     (LPMS_DATA_BASE + 23)
#define GYR_Z_0                     (LPMS_DATA_BASE + 24)
#define GYR_Z_1                     (LPMS_DATA_BASE + 25)
#define GYR_Z_2                     (LPMS_DATA_BASE + 26)
#define GYR_Z_3                     (LPMS_DATA_BASE + 27)
#define MAG_X_0                     (LPMS_DATA_BASE + 28)
#define MAG_X_1                     (LPMS_DATA_BASE + 29)
#define MAG_X_2                     (LPMS_DATA_BASE + 30)
#define MAG_X_3                     (LPMS_DATA_BASE + 31)
#define MAG_Y_0                     (LPMS_DATA_BASE + 32)
#define MAG_Y_1                     (LPMS_DATA_BASE + 33)
#define MAG_Y_2                     (LPMS_DATA_BASE + 34)
#define MAG_Y_3                     (LPMS_DATA_BASE + 35)
#define MAG_Z_0                     (LPMS_DATA_BASE + 36)
#define MAG_Z_1                     (LPMS_DATA_BASE + 37)
#define MAG_Z_2                     (LPMS_DATA_BASE + 38)
#define MAG_Z_3                     (LPMS_DATA_BASE + 39)
#define EULER_X_0                   (LPMS_DATA_BASE + 40)
#define EULER_X_1                   (LPMS_DATA_BASE + 41)
#define EULER_X_2                   (LPMS_DATA_BASE + 42)
#define EULER_X_3                   (LPMS_DATA_BASE + 43)
#define EULER_Y_0                   (LPMS_DATA_BASE + 44)
#define EULER_Y_1                   (LPMS_DATA_BASE + 45)
#define EULER_Y_2                   (LPMS_DATA_BASE + 46)
#define EULER_Y_3                   (LPMS_DATA_BASE + 47)
#define EULER_Z_0                   (LPMS_DATA_BASE + 48)
#define EULER_Z_1                   (LPMS_DATA_BASE + 49)
#define EULER_Z_2                   (LPMS_DATA_BASE + 50)
#define EULER_Z_3                   (LPMS_DATA_BASE + 51)
#define QUAT_W_0                    (LPMS_DATA_BASE + 52)
#define QUAT_W_1                    (LPMS_DATA_BASE + 53)
#define QUAT_W_2                    (LPMS_DATA_BASE + 54)
#define QUAT_W_3                    (LPMS_DATA_BASE + 55)
#define QUAT_X_0                    (LPMS_DATA_BASE + 56)
#define QUAT_X_1                    (LPMS_DATA_BASE + 57)
#define QUAT_X_2                    (LPMS_DATA_BASE + 58)
#define QUAT_X_3                    (LPMS_DATA_BASE + 59)
#define QUAT_Y_0                    (LPMS_DATA_BASE + 60)
#define QUAT_Y_1                    (LPMS_DATA_BASE + 61)
#define QUAT_Y_2                    (LPMS_DATA_BASE + 62)
#define QUAT_Y_3                    (LPMS_DATA_BASE + 63)
#define QUAT_Z_0                    (LPMS_DATA_BASE + 64)
#define QUAT_Z_1                    (LPMS_DATA_BASE + 65)
#define QUAT_Z_2                    (LPMS_DATA_BASE + 66)
#define QUAT_Z_3                    (LPMS_DATA_BASE + 67)
#define LIN_ACC_X_0                 (LPMS_DATA_BASE + 68)
#define LIN_ACC_X_1                 (LPMS_DATA_BASE + 69)
#define LIN_ACC_X_2                 (LPMS_DATA_BASE + 70)
#define LIN_ACC_X_3                 (LPMS_DATA_BASE + 71)
#define LIN_ACC_Y_0                 (LPMS_DATA_BASE + 72)
#define LIN_ACC_Y_1                 (LPMS_DATA_BASE + 73)
#define LIN_ACC_Y_2                 (LPMS_DATA_BASE + 74)
#define LIN_ACC_Y_3                 (LPMS_DATA_BASE + 75)
#define LIN_ACC_Z_0                 (LPMS_DATA_BASE + 76)
#define LIN_ACC_Z_1                 (LPMS_DATA_BASE + 77)
#define LIN_ACC_Z_2                 (LPMS_DATA_BASE + 78)
#define LIN_ACC_Z_3                 (LPMS_DATA_BASE + 79)
#define TEMP_0                      (LPMS_DATA_BASE + 80)
#define TEMP_1                      (LPMS_DATA_BASE + 81)
#define TEMP_2                      (LPMS_DATA_BASE + 82)
#define TEMP_3                      (LPMS_DATA_BASE + 83)

//System information registers
#define LP_WHO_AM_I                 (LPMS_INFO_BASE + 0)
#define FIRMWARE_VER_0              (LPMS_INFO_BASE + 1)
#define FIRMWARE_VER_1              (LPMS_INFO_BASE + 2)

/*
  *}System Registers
  */

/*
  *Register Function{
  */
//Function config
#define LPMS_FUN_CONFIG_MASK                    ((uint8_t)0x80)

#define LPMS_FUN_CFG_EN                         ((uint8_t)0x01<<7)


//System config
#define LPMS_SYS_CONFIG_MASK                    ((uint8_t)0xC1)

#define LPMS_LED_EN                             ((uint8_t)0x01<<0)
#define LPMS_SYS_REBOOT                         ((uint8_t)0x01<<6)
#define LPMS_SYS_RESET                          ((uint8_t)0x01<<7)


//Data control
#define LPMS_DATA_CTRL_MASK                     ((uint8_t)0x7F)

#define LPMS_DATA_FREQ_MASK                     ((uint8_t)0x0F)
#define LPMS_DATA_FREQ_5HZ                      ((uint8_t)0x00)
#define LPMS_DATA_FREQ_10HZ                     ((uint8_t)0x01)
#define LPMS_DATA_FREQ_50HZ                     ((uint8_t)0x02)
#define LPMS_DATA_FREQ_100HZ                    ((uint8_t)0x03)
#define LPMS_DATA_FREQ_200HZ                    ((uint8_t)0x04)
#define LPMS_DATA_FREQ_400HZ                    ((uint8_t)0x05)
#define LPMS_DATA_FREQ_500HZ                    ((uint8_t)0x06)
#define LPMS_DATA_FREQ_800HZ                    ((uint8_t)0x07)
#define LPMS_DATA_FREQ_1000HZ                   ((uint8_t)0x08)

#define LPMS_DATA_FORMAT                        ((uint8_t)0x01<<4)
#define LPMS_DATA_FORMAT_32BIT_FLOAT            ((uint8_t)0x00<<4)
#define LPMS_DATA_FORMAT_16BIT_UINT             ((uint8_t)0x01<<4)

#define LPMS_RESET_TIME                         ((uint8_t)0x01<<5)

//Data enable
#define LPMS_DATA_EN_MASK                       ((uint8_t)0xFF)

#define LPMS_TIME_EN                            ((uint8_t)0x01<<0)
#define LPMS_ACC_EN                             ((uint8_t)0x01<<1)
#define LPMS_GYR_EN                             ((uint8_t)0x01<<2)
#define LPMS_MAG_EN                             ((uint8_t)0x01<<3)
#define LPMS_EULER_EN                           ((uint8_t)0x01<<4)
#define LPMS_QUAT_EN                            ((uint8_t)0x01<<5)
#define LPMS_LINACC_EN                          ((uint8_t)0x01<<6)
#define LPMS_TEMP_EN                            ((uint8_t)0x01<<7)
#define LPMS_ALL_DATA_EN                        ((uint8_t)0xFF)

//CONTROL 0 ACC
#define LPMS_ACC_MASK                           ((uint8_t)0x0C)

#define LPMS_ACC_FS_POS                         ((uint8_t)0x02)
#define LPMS_ACC_FS_MASK                        ((uint8_t)0x03<<2)
#define LPMS_ACC_FS_2G                          ((uint8_t)0x00<<2)
#define LPMS_ACC_FS_16G                         ((uint8_t)0x01<<2)
#define LPMS_ACC_FS_4G                          ((uint8_t)0x02<<2)
#define LPMS_ACC_FS_8G                          ((uint8_t)0x03<<2)


//CONTROL 1 GYR
#define LPMS_GYR_MASK                           ((uint8_t)0x8E)

#define LPMS_GYR_FS_POS                         ((uint8_t)0x01)
#define LPMS_GYR_FS_MASK                        ((uint8_t)0x07<<1)
#define LPMS_GYR_FS_125DPS                      ((uint8_t)0x01<<1)
#define LPMS_GYR_FS_245DPS                      ((uint8_t)0x00<<1)
#define LPMS_GYR_FS_500DPS                      ((uint8_t)0x02<<1)
#define LPMS_GYR_FS_1000DPS                     ((uint8_t)0x04<<1)
#define LPMS_GYR_FS_2000DPS                     ((uint8_t)0x06<<1)
#define LPMS_GYR_CALIBRA                        ((uint8_t)0x01<<7)


//CONTROL 2 MAG
#define LPMS_MAG_MASK                           ((uint8_t)0xE0)

#define LPMS_MAG_FS_POS                         ((uint8_t)0x05)
#define LPMS_MAG_FS_MASK                        ((uint8_t)0x03<<5)
#define LPMS_MAG_FS_4GAUSS                      ((uint8_t)0x00<<5)
#define LPMS_MAG_FS_8GAUSS                      ((uint8_t)0x01<<5)
#define LPMS_MAG_FS_12GAUSS                     ((uint8_t)0x02<<5)
#define LPMS_MAG_FS_16GAUSS                     ((uint8_t)0x03<<5)
#define LPMS_MAG_CALIBRA                        ((uint8_t)0x01<<7)


//Status
#define LPMS_STATUS_MASK                        ((uint8_t)0x19)

#define LPMS_DATA_READY                         ((uint8_t)0x01<<0)
#define LPMS_GYR_IS_CALIBRATING                 ((uint8_t)0x01<<4)
#define LPMS_MAG_IS_CALIBRATING                 ((uint8_t)0x01<<3)


//Filter config
#define LPMS_FILTER_CINFIG_MASK                 ((uint8_t)0x3F)

#define LPMS_FILTER_MODE_MASK                   ((uint8_t)0x07)
#define LPMS_FILTER_MODE_GYR                    ((uint8_t)0x00)
#define LPMS_FILTER_MODE_GYR_ACC                ((uint8_t)0x01)
#define LPMS_FILTER_MODE_GYR_ACC_MAG            ((uint8_t)0x02)
#define LPMS_FILTER_MODE_MAD_GYR_ACC            ((uint8_t)0x03)
#define LPMS_FILTER_MODE_MAD_GYR_ACC_MAG        ((uint8_t)0x04)

#define LPMS_LOW_PASS_MASK                      ((uint8_t)0x07<<3)
#define LPMS_LOW_PASS_OFF                       ((uint8_t)0x00<<3)
#define LPMS_LOW_PASS_ALPHA_01                  ((uint8_t)0x01<<3)
#define LPMS_LOW_PASS_ALPHA_005                 ((uint8_t)0x02<<3)
#define LPMS_LOW_PASS_ALPHA_001                 ((uint8_t)0x03<<3)
#define LPMS_LOW_PASS_ALPHA_0005                ((uint8_t)0x04<<3)
#define LPMS_LOW_PASS_ALPHA_0001                ((uint8_t)0x05<<3)

//Offset Setting
#define LPMS_OFFSET_SETTING_MASK                ((uint8_t)0x07)
#define LPMS_OBJECT_RESET                       ((uint8_t)0x01<<0)
#define LPMS_HEADING_RESET                      ((uint8_t)0x01<<1)
#define LPMS_RESET_OFFSET                       ((uint8_t)0x01<<2)

typedef enum {
    ME1_ERROR = 0,
    ME1_OK = 1
} me1_status_t;

typedef union {
    uint8_t u8vals[4];
    uint32_t u32val;
    float fval;
} data_decoder;

typedef struct
{
    float time;
    float acc[3];
    float gyr[3];
    float mag[3];
    float euler[3];
    float quat[4];
    float linAcc[3];
    float temp;
    uint8_t firmware_version[2];
} sensor_data_t;


class LPMS_ME1
{
public:
    LPMS_ME1();
    
    void setup(void);

    void set_config_enable(void);
    void set_config_disable(void);
    void set_data_output(uint8_t data_en);
    me1_status_t set_data_freq(uint8_t freq);
    me1_status_t set_acc_range(uint8_t range);
    me1_status_t set_gyr_range(uint8_t range);
    me1_status_t set_mag_rang(uint8_t range);



    void get_time(float *ts);
    void get_acc(float *acc);
    void get_gyr(float *gyr);
    void get_mag(float *mag);
    void get_quat(float *quat);
    void get_euler(float *euler);
    void get_linacc(float *linacc);
    void get_temp(float *temp);
    void get_firmware_version(uint8_t *firmware_version);
    void get_reg_data(uint8_t addr, uint8_t *buf, uint8_t len);

private:
    float uint8_to_float(uint8_t *pu8vals);
    uint32_t uint8_to_uint32(uint8_t *pu8vals);

    // low level functions
    void read_register(uint8_t addr, uint8_t *buf, uint8_t len);
    void write_register(uint8_t addr, uint8_t *buf, uint8_t len);
    void wait_update(void);
    void wait_reset(void);
    void wait_reboot(void);  
};
#endif
